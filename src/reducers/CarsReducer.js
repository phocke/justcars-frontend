import {
  FETCH_CARS_BEGIN,
  FETCH_CARS_SUCCESS,
  FETCH_CARS_FAILURE,
  FETCH_CAR_BEGIN,
  FETCH_CAR_SUCCESS,
  FETCH_CAR_FAILURE,
  UPDATE_CURRENT_CAR
} from "../actions/CarsActions";

const initialState = {
  cars: [],
  currentCar: null,
  loading: false,
  error: null
};

export default function carsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CARS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_CARS_SUCCESS:
      return {
        ...state,
        loading: false,
        cars: action.payload.cars
      };

    case FETCH_CARS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        cars: []
      };
    case FETCH_CAR_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_CAR_SUCCESS:
      return {
        ...state,
        loading: false,
        car: action.payload
      };

    case FETCH_CAR_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        car: null
      };

    case UPDATE_CURRENT_CAR:
      return {
        ...state,
        currentCar: action.payload.currentCar
      };

    default:
      return state;
  }
}
