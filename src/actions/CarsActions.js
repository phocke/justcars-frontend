import { API_URL } from "../constants";

export const FETCH_CARS_BEGIN = "FETCH_CARS_BEGIN";
export const FETCH_CARS_SUCCESS = "FETCH_CARS_SUCCESS";
export const FETCH_CARS_FAILURE = "FETCH_CARS_FAILURE";

export const FETCH_CAR_BEGIN = "FETCH_CAR_BEGIN";
export const FETCH_CAR_SUCCESS = "FETCH_CAR_SUCCESS";
export const FETCH_CAR_FAILURE = "FETCH_CAR_FAILURE";

export const UPDATE_CURRENT_CAR = "UPDATE_CURRENT_CAR";

export const fetchCarsBegin = (priceMin = 0, priceMax = 1000000) => ({
  type: FETCH_CARS_BEGIN,
  payload: { priceMin: priceMin, priceMax: priceMax }
});

export const fetchCarsSuccess = cars => ({
  type: FETCH_CARS_SUCCESS,
  payload: { cars }
});

export const fetchCarsFailure = error => ({
  type: FETCH_CARS_FAILURE,
  payload: { error }
});

export const fetchCarBegin = () => ({
  type: FETCH_CAR_BEGIN
});

export const fetchCarSuccess = car => ({
  type: FETCH_CAR_SUCCESS,
  payload: car
});

export const fetchCarFailure = error => ({
  type: FETCH_CAR_FAILURE,
  payload: { error }
});

export const updateCurrentCarSuccess = currentCar => ({
  type: UPDATE_CURRENT_CAR,
  payload: { currentCar: currentCar }
});

export function fetchCars(priceMin = 0, priceMax = 1000000) {
  return dispatch => {
    dispatch(fetchCarsBegin());
    return fetch(`${API_URL}/cars?price_min=${priceMin}&price_max=${priceMax}`)
      .then(handleErrors)
      .then(response => response.json())
      .then(cars => {
        dispatch(fetchCarsSuccess(cars));
        return cars;
      })
      .catch(error => dispatch(fetchCarsFailure(error)));
  };
}

export function fetchCar(carId) {
  return dispatch => {
    dispatch(fetchCarBegin());
    return fetch(`${API_URL}/cars/${carId}`)
      .then(handleErrors)
      .then(response => response.json())
      .then(car => {
        dispatch(fetchCarSuccess(car));
        return car;
      })
      .catch(error => dispatch(fetchCarFailure(error)));
  };
}

export function updateCurrentCar(currentCar) {
  return dispatch => {
    dispatch(updateCurrentCarSuccess(currentCar));
    return currentCar;
  };
}

function handleErrors(response) {
  if (!response.ok) {
    console.log(Error(response.statusText));
  }
  return response;
}
