import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { updateCurrentCar } from "../actions/CarsActions";

function CarDescription(props) {
  const car = props.car;
  const isOpen = props.isOpen;

  if (!isOpen) {
    return <></>;
  }

  return (
    <>
      <p>{car.description.split("\n").slice(0, 1)}</p>
      <Link className="primary" to={`/cars/${car.id}`}>
        Show full ad
      </Link>
    </>
  );
}

class CarsEntry extends React.Component {
  state = {
    isOpen: false
  };

  onToggle = e => {
    e.preventDefault();

    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));

    this.props.dispatch(updateCurrentCar(this.props.car));
  };

  render() {
    const { car } = this.props;
    const { id, title } = car;
    const isOpen = this.state.isOpen;

    return (
      <li className={isOpen ? "active" : ""}>
        <p>
          <Link to={`/cars/${id}`} onClick={this.onToggle}>
            {title.slice(0, 30)}
          </Link>
        </p>

        <CarDescription car={car} isOpen={isOpen} />
      </li>
    );
  }
}

const mapStateToProps = state => ({
  currentCar: state.car
});

export default connect(mapStateToProps)(CarsEntry);
