import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetchCar } from "../actions/CarsActions";

class CarFull extends React.Component {
  componentDidMount() {
    const carId = this.props.match.params.id;
    this.props.dispatch(fetchCar(carId));
  }

  componentDidUpdate(prevProps, prevState) {
    const locationChanged = prevProps.location !== this.props.location;
    if (locationChanged) {
      const carId = this.props.match.params.id;
      this.props.dispatch(fetchCar(carId));
    }
  }

  render() {
    const { error, loading, car } = this.props;

    if (error) {
      return (
        <main className="full-car">
          <p>{error.message}</p>
        </main>
      );
    }

    if (loading) {
      return (
        <main className="full-car">
          <p>Loading ...</p>
        </main>
      );
    }

    if (car) {
      return (
        <>
          <nav className="car-filters">
            <Link onClick={this.props.history.goBack}>
              {"<= Go back to cars list"}
            </Link>
          </nav>

          <main className="full-car">
            <h3>{car.title}</h3>
            <img src={`${car.photo_url}?rand=${Math.random()}`} alt="car" />
            {car.description.split("\n").map(p => (
              <p>{p}</p>
            ))}
            <p>
              Price: <b>${car.price}</b>
            </p>
          </main>
        </>
      );
    }

    return (
      <>
        <nav className="car-filters">
          <Link onClick={this.props.history.goBack}>Go back to cars list</Link>
        </nav>
        <main className="full-car">
          <p>{JSON.stringify(car)}</p>
        </main>
      </>
    );
  }
}

const mapStateToProps = state => ({
  car: state.car,
  loading: state.loading,
  error: state.error
});

export default connect(mapStateToProps)(CarFull);
