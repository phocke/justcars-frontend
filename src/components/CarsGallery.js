import React from "react";
import { connect } from "react-redux";

class CarsGallery extends React.Component {
  render() {
    const currentCar = this.props.currentCar;

    return (
      <main>
        {currentCar ? (
          <>
            <img
              src={`${currentCar.photo_url}?rand=${Math.random()}`}
              alt="car"
            />
          </>
        ) : (
          <p>
            <b>Select a car from the list ==========></b>
          </p>
        )}
      </main>
    );
  }
}

const mapStateToProps = state => ({
  currentCar: state.currentCar
});
export default connect(mapStateToProps)(CarsGallery);
