import React from "react";
import { connect } from "react-redux";
import { fetchCars } from "../actions/CarsActions";
import CarEntry from "./CarEntry";
import CarsGallery from "./CarsGallery";

class CarsList extends React.Component {
  componentDidMount() {
    this.props.dispatch(fetchCars());
  }

  render() {
    const { error, loading, cars } = this.props;

    if (error) {
      return <aside>Error! {error.message}</aside>;
    }

    if (loading) {
      return <aside>Loading...</aside>;
    }

    return (
      <>
        <CarsGallery />
        <aside>
          <ul>
            {cars.length > 0 ? (
              cars.map(car => <CarEntry key={car.id} car={car} />)
            ) : (
              <p>no cars match your criteria</p>
            )}
          </ul>
        </aside>
      </>
    );
  }
}

const mapStateToProps = state => ({
  cars: state.cars,
  loading: state.loading,
  error: state.error
});

export default connect(mapStateToProps)(CarsList);
