import React from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { fetchCars, updateCurrentCar } from "../actions/CarsActions";

class CarFilters extends React.Component {
  constructor(props) {
    super(props);

    const params = new URLSearchParams(window.location.search);
    const priceMin = params.has("priceMin") ? params.get("priceMin") : 0;
    const priceMax = params.has("priceMax") ? params.get("priceMax") : 1000000;
    this.componentDidUpdate = this.componentDidUpdate.bind(this);

    this.state = {
      priceMin: priceMin,
      priceMax: priceMax
    };
  }

  componentDidUpdate(prevProps, prevState) {
    const locationChanged = prevState !== this.state;

    if (locationChanged) {
      this.props.history.push({
        pathname: "/cars",
        search:
          "?" +
          new URLSearchParams({
            priceMin: this.state.priceMin,
            priceMax: this.state.priceMax
          }).toString()
      });
      this.props.dispatch(fetchCars(this.state.priceMin, this.state.priceMax));
      this.props.dispatch(updateCurrentCar(null));
    }
  }

  render() {
    return (
      <nav className="car-filters">
        <form method="get" ref="search-form">
          <fieldset>
            <legend>Your search criteria:</legend>
            <label htmlFor="min">Price from:</label>
            <input
              type="number"
              id="price-min"
              name="priceMin"
              defaultValue={this.state.priceMin}
              onBlur={e => this.setState({ priceMin: e.target.value })}
            />
            <label htmlFor="max">Price to:</label>
            <input
              type="number"
              id="price-max"
              name="priceMax"
              defaultValue={this.state.priceMax}
              onBlur={e => this.setState({ priceMax: e.target.value })}
            />
          </fieldset>
        </form>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  cars: state.cars,
  loading: state.loading,
  error: state.error
});

export default withRouter(connect(mapStateToProps)(CarFilters));
