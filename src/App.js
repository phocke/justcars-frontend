import React from "react";
import CarFilters from "./components/CarFilters";
import CarsList from "./components/CarsList";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CarFull from "./components/CarFull";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header>
          <a href="/">
            <h2 className="rainbow-text">JustCars.it</h2>
          </a>
        </header>
        <Router>
          <Switch>
            <Route exact path="/(cars|)">
              <CarFilters />
              <CarsList />
            </Route>
            <Route exact path="/cars/:id" component={CarFull} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
